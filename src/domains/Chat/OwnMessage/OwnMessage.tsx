import React from 'react';

import clsx from 'clsx';

import './OwnMessage.scss';

import { Message, MessageProps } from '../Message';

export type OwnMessageProps = MessageProps;

export const OwnMessage: React.FC<OwnMessageProps> = ({
	className,
	...restProps
}) => {
	return (
		<Message
			withAvatar={false}
			withLike={false}
			withRemove
			withEdit
			className={clsx(className, 'own-message')}
			{...restProps}
		/>
	);
};
