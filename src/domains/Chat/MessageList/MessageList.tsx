import React, { HTMLAttributes } from 'react';

import clsx from 'clsx';

import './MessageList.scss';

import { Message } from '../Message';
import { OwnMessage } from '../OwnMessage';

export type MessageListProps = HTMLAttributes<HTMLDivElement>;

export const MessageList: React.FC<MessageListProps> = ({
	className,
	...restDivProps
}) => {
	return (
		<div className={clsx('message-list', className)} {...restDivProps}>
			{[...Array(100)].map((_, i) => (
				<>
					<Message key={i + 'i'} className='message-item' />
					<OwnMessage key={i} className='message-item' />
				</>
			))}
		</div>
	);
};
