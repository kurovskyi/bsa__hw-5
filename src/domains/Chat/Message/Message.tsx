import React, { HTMLAttributes } from 'react';

import clsx from 'clsx';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHeart as faHeartSolid } from '@fortawesome/free-solid-svg-icons';
import {
	faHeart as faHeartRegular,
	faEdit,
	faTrashAlt,
} from '@fortawesome/free-regular-svg-icons';

import './Message.scss';

export type MessageProps = HTMLAttributes<HTMLDivElement> & {
	withAvatar?: boolean;
	withLike?: boolean;
	withRemove?: boolean;
	withEdit?: boolean;
};

export const Message: React.FC<MessageProps> = ({
	withAvatar = true,
	withLike = true,
	withRemove,
	withEdit,
	className,
	...divProps
}) => {
	return (
		<div className={clsx(className, 'message')} {...divProps}>
			{withAvatar && (
				<div className='message-avatar-wrap'>
					<img
						src='https://i.pinimg.com/originals/51/f6/fb/51f6fb256629fc755b8870c801092942.png'
						alt=''
						className='message-user-avatar'
					/>
				</div>
			)}
			<div className='message-wrap'>
				<span className='message-user-name'>Dmytro Kurovskyi</span>
				<span className='message-text'>Message text</span>
				<div className='message-bottom-block'>
					<span className='message-time'>12:30</span>
					{withLike && <FontAwesomeIcon icon={faHeartRegular} />}
					{withEdit && <FontAwesomeIcon icon={faEdit} />}
					{withRemove && <FontAwesomeIcon icon={faTrashAlt} />}
				</div>
			</div>
		</div>
	);
};
