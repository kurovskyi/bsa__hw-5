import React from 'react';

import './Header.scss';

export const Header: React.FC = () => {
	return (
		<header className='header'>
			<div className='header-left-container'>
				<span className='header-title'>My Chat</span>
				<span className='header-users-count'>10 participants</span>
				<span className='header-messages-count'>30 participants</span>
			</div>
			<div className='header-right-container'>
				<span className='header-last-message-date'>Last message at 14:23</span>
			</div>
		</header>
	);
};
