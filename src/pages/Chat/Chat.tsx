import React from 'react';

import { Header } from '../../domains/Chat/Header';
import { MessageList } from '../../domains/Chat/MessageList';

import './Chat.scss';

export type ChatProps = {
	url: string;
};

export const Chat: React.FC<ChatProps> = ({ url }) => {
	return (
		<div className='chat'>
			<Header />
			<MessageList style={{ flex: 1 }} />
		</div>
	);
};
